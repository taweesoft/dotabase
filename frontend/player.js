import React from 'react';
import ReactDOM from 'react-dom';
import Table from 'react-bootstrap/lib/Table';
import Radium from 'radium';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import DataTable from './table.js';
import Constants from './constants';
import contentStyle from './css/contentStyle';
import tableStyle from './css/tableStyle';
import Hero from './hero';

class Player extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data : {},
      played_heroes : [],
      key : 0
    }
    this.header = ["","Hero name" , "Win / Lose" , "Kill"  , "Death" , "Assist" , "GPM" , "XPM"];

    var parameter = {id : this.props.id}
    $.ajax({
      data : JSON.stringify(parameter),
      url : Constants.url+"getPlayerInfo",
      contentType : "application/json",
      type : "POST",
      success : (result) => {
        this.setState({
          data : result.player
        })
      }
    });

    $.ajax({
      data : JSON.stringify(parameter),
      url: Constants.url + "getPlayedHero",
      contentType : "application/json",
      type : "POST",
      success : (result) => {
        this.setState({
          played_heroes : result.played_hero,
          key : 1
        })
      }
    })
  }

  clicked(item) {
    ReactDOM.render(<Hero id={item.hero_id}/> , document.getElementById('content'));
  }

  render() {
    var self = this;
    var profileImageStyle = {
      marginTop : 20,
      width : 130,
      height : 130,
      borderRadius : "50%"
    }

    var flagImgStyle = {
      width : 57,
      height : 40,
      borderRadius : "50%"
    }
    var teamLogoStyle = {
      width : 40,
      height : 40,
      marginRight : 15
    }

    var spanNormalStyle = {
      fontSize : 17,
      verticalAlign : "middle"
    }

    var profile = this.state.data;

    var tableDataFunc = (item , i) => {
      var winLoseStyle = {
        color : item.win_lose == 'lose' ? "#F44336" : "#7ED321"
      }
      return (
        <tr key={i} id="trDataRow" style={tableStyle.trDataStyle.base} onClick={() => self.clicked(item)}>
          <td style={tableStyle.tdStyle}><img width={55} height={30} src={item.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.hero_name}</td>
          <td style={[tableStyle.tdStyle,winLoseStyle]}>{item.win_lose}</td>
          <td style={tableStyle.tdStyle}>{item.kills}</td>
          <td style={tableStyle.tdStyle}>{item.death}</td>
          <td style={tableStyle.tdStyle}>{item.assist}</td>
          <td style={tableStyle.tdStyle}>{item.gpm}</td>
          <td style={tableStyle.tdStyle}>{item.xpm}</td>
        </tr>
      );
    }

    return (
        <ReactCSSTransitionGroup transitionName = "example"
               transitionAppear = {true} transitionAppearTimeout = {500}
               transitionEnter = {true} transitionLeave = {true}>
              <div style={contentStyle.divStyle}>
               <span>Information</span>
               <div style={contentStyle.dividerStyle}></div>
               <div style={{textAlign : "center"}}>
                 <img id="profileImage" src={profile.image_url} style={profileImageStyle} ></img>
                 <span style={contentStyle.nameStyle}>{profile.firstname + " " + profile.lastname}</span>
               </div>
               <div style={contentStyle.divBlockWrapperStyle}>

                 <div id="ingame_name" style={contentStyle.divInfoBlockStyle}>
                 <span>Ingame name</span>
                 <div id="divider" style={contentStyle.dividerStyle}></div>
                 <div style={contentStyle.divInfoSectionStyle}>
                   <span style={spanNormalStyle}>{profile.ingame_name}</span>
                 </div>
                 </div>

                 <div id="nation" style={contentStyle.divInfoBlockStyle}>
                 <span>Nationality</span>
                 <div id="divider" style={contentStyle.dividerStyle}></div>
                 <div style={contentStyle.divInfoSectionStyle}>
                   <img style={flagImgStyle} src={profile.flag_image_url}></img> {/*flcontentStyle.ag img*/}
                   <span style={spanNormalStyle}>{profile.nation}</span>
                 </div>
                 </div>

                 <div id="position" style={contentStyle.divInfoBlockStyle}>
                 <span>Position</span>
                 <div id="divider" style={contentStyle.dividerStyle}></div>
                 <div style={contentStyle.divInfoSectionStyle}>
                   <span style={spanNormalStyle} >{profile.player_position}</span>
                 </div>
                 </div>
               </div>

               <div style={contentStyle.divBlockWrapperStyle}>

                 <div id="team" style={contentStyle.divInfoBlockStyle}>
                 <span>Team</span>
                 <div id="divider" style={contentStyle.dividerStyle}></div>
                 <div style={contentStyle.divInfoSectionStyle}>
                   <img style={teamLogoStyle} src={profile.logo_url}></img>
                   <span style={spanNormalStyle}>{profile.team_name}</span>
                 </div>
                 </div>

                 <div id="winrate" style={contentStyle.divInfoBlockStyle}>
                 <span>Winrate</span>
                 <div id="divider" style={contentStyle.dividerStyle}></div>
                 <div style={contentStyle.divInfoSectionStyle}>
                   <span style={contentStyle.spanNormalStyle}>{profile.winrate}</span>
                 </div>
                 </div>

                 <div id="winlose" style={contentStyle.divInfoBlockStyle}>
                 <span><span style={{color : "#7ED321"}}>Win</span> | <span style={{color : "#F95B45"}}>Lose</span></span>
                 <div id="divider" style={contentStyle.dividerStyle}></div>
                 <div style={contentStyle.divInfoSectionStyle}>
                   <span style={contentStyle.spanNormalStyle}><span style={{color : "#7ED321"}}>{profile.win}</span> | <span style={{color : "#F95B45"}}>{profile.lose}</span></span>
                 </div>
                 </div>
               </div>

               <div style={{marginTop : 30}}>
               <DataTable key={this.state.key} header={this.header} data={this.state.played_heroes} func={tableDataFunc} />
               </div>
              </div>
        </ReactCSSTransitionGroup>

    );
   }
}

export default Radium(Player);

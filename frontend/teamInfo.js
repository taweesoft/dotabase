import React from 'react';
import ReactDOM from 'react-dom';
import Radium from 'radium';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Table from 'react-bootstrap/lib/Table';
import DataTable from './table.js';
import Player from './player.js';
import Constants from './constants';
import contentStyle from './css/contentStyle';
import tableStyle from './css/tableStyle';

class TeamInfo extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        data : {},
        members : [],
        key : 0
      }
      this.header = ["" , "Name" , "Ingame name" , "Nationality" , "Position"];
      this.loadData = this.loadData.bind(this);
      var teamInfoSuccess = (result) => {
        this.setState({
          data : result.team
        })
      }
      var teamMemberSuccess = (result) => {
        this.setState({
          members : result.members,
          key : 1
        })
      }

      this.loadData(teamInfoSuccess , "getTeamInfo");
      this.loadData(teamMemberSuccess , "getTeamMembers");
  }

  loadData(successFunc , route) {
    var parameter = {id : this.props.id};
    $.ajax({
      url : Constants.url+route,
      data : JSON.stringify(parameter),
      type : "POST",
      contentType : "application/json",
      success : successFunc
    });
  }

  clicked(item) {
    console.log("id from teaminfo : " + item.id);
    ReactDOM.render(<Player id={item.id} /> , document.getElementById('content'));
  }

  render() {
    var self = this;
    var logoStyle = {
      width : 40,
      height : 40,
      marginRight : 15
    }

    var team = this.state.data;

    var f = (item , i ) => {
      var winLoseStyle = {
        color : item.win_lose == 'lose' ? "#F44336" : "#7ED321"
      }

      var imageStyle= {
        width : 40,
        heifht : 40,
        borderRadius : "50%"
      }
      return (
        <tr onClick={()=>self.clicked(item)} key={i} id="trDataRow" style={tableStyle.trDataStyle.base}>
          <td style={tableStyle.tdStyle}><img style={imageStyle} src={item.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.firstname + " " + item.lastname}</td>
          <td style={tableStyle.tdStyle}>{item.ingame_name}</td>
          <td style={tableStyle.tdStyle}>{item.nation}</td>
          <td style={tableStyle.tdStyle}>{item.player_position}</td>
        </tr>
      );
    }

    var flagImgStyle = {
      width : 40,
      height : 40,
      borderRadius : "50%",
      marginRight : 15
    }

    var divInfoSectionStyle = {
      textAlign : "center",
      marginTop : 20,
      marginBottom : 20,
      verticalAlign : "middle",
      height : "auto"
    }

    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
             <div style={contentStyle.divStyle}>
               <span>Information</span>
               <div style={contentStyle.dividerStyle}></div>
               <div style={{textAlign : "center"}}>
                <img style={contentStyle.centerRectangleImageStyle} src={team.image_url}></img>
                <div style={{marginTop : 20}}>
                  <img style={logoStyle} src={team.logo_url}></img>
                  <span>{team.team_name}</span>
                </div>
               </div>
               <div style={{marginTop : 20}}>
                <span>Members</span>
                <div style={[contentStyle.dividerStyle , {marginBottom : 20}]}></div>
                <DataTable key={this.state.key} data={this.state.members} func={f} header={this.header}/>
                <div style={contentStyle.divBlockWrapperStyle}>

                  <div id="position" style={contentStyle.divInfoBlockStyle}>
                  <span>Ranking</span>
                  <div id="divider" style={contentStyle.dividerStyle}></div>
                  <div style={divInfoSectionStyle}>
                    <span style={contentStyle.spanNormalStyle}>{team.position}</span>
                  </div>
                  </div>

                  <div id="winrate" style={contentStyle.divInfoBlockStyle}>
                  <span>Winrate</span>
                  <div id="divider" style={contentStyle.dividerStyle}></div>
                  <div style={divInfoSectionStyle}>
                    <span style={contentStyle.spanNormalStyle}>{team.winrate}</span>
                  </div>
                  </div>

                  <div id="winlose" style={contentStyle.divInfoBlockStyle}>
                  <span><span style={{color : "#7ED321"}}>Win</span> | <span style={{color : "#F95B45"}}>Lose</span></span>
                  <div id="divider" style={contentStyle.dividerStyle}></div>
                  <div style={divInfoSectionStyle}>
                    <span style={contentStyle.spanNormalStyle}><span style={{color : "#7ED321"}}>{team.win}</span> | <span style={{color : "#F95B45"}}>{team.lose}</span></span>
                  </div>
                  </div>
                </div>
               </div>
             </div>
      </ReactCSSTransitionGroup>
    );
  }
}

export default Radium(TeamInfo);

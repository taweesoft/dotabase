import React from 'react';
import Radium from 'radium';
import contentStyle from './css/contentStyle';
import Constants from './constants';
import Col from 'react-bootstrap/lib/Col';
import tableStyle from './css/tableStyle';
import DataTable from './table';
import FormControl from 'react-bootstrap/lib/FormControl';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import moment from 'moment';
class Prediction extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      data : [],
      key : 0,
      currentPlayer : {},
      radiant_winrate : 0.00,
      dire_winrate : 0.00,
      radiant : [],
      dire : [],
      players : [],
      backupPlayersData : [],
      played_heroes : [],
      playerIndex : -1,
      showModal : false,
      predictionHistory : [],
      pickedHero : []
    }
    var self = this;
    $.get(Constants.url + "getPlayers" , function(result) {
      self.setState({
        players : result.players,
        backupPlayersData : result.players,
        key : self.state.key+1
      })
    });
    this.count = 0;
    this.maxPlayer = 10;
    this.heroPicked = this.heroPicked.bind(this);
    this.playerPicked = this.playerPicked.bind(this);
    this.getWinrate = this.getWinrate.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.showModal = this.showModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.historyClicked = this.historyClicked.bind(this);
    this.reset = this.reset.bind(this);
  }

  heroPicked(item) {
    var data = {
      player : this.state.currentPlayer,
      hero : item
    }
    var self = this;
    var playerTemp = this.state.backupPlayersData;
    if(this.count >= this.maxPlayer){
      if(this.count == 100)
        this.getWinrate(false);
      else
        this.getWinrate(true);
      return;
    }
    var index = -1;
    playerTemp.forEach((item,i)=> {
      if(item.id == self.state.currentPlayer.id){
        index = i;
      }
    });
    playerTemp.splice(index,1);
    var heroPickedTemp = this.state.pickedHero;
    heroPickedTemp.push(item);
    this.setState({
      backupPlayersData : playerTemp,
      players : playerTemp
    });
    if(this.count < this.maxPlayer/2){
      var radiantTemp = self.state.radiant;
      radiantTemp.push(data);
      this.setState({
        radiant : radiantTemp,
        played_heroes : [],
        playerIndex : -1,
        key : self.state.key+1
      })
    }else{
      var direTemp = self.state.dire;
      direTemp.push(data);
      this.setState({
        dire : direTemp,
        played_heroes : [],
        playerIndex : -1,
        key : self.state.key+1
      })
    }


    this.count += 1;
    if(this.count >= this.maxPlayer){
      this.getWinrate(true);
    }
  }

  playerPicked(item,i) {
    var self = this;
    var parameter = {id : item.id}
    $.ajax({
      url : Constants.url + "getPlayedHeroDistinct",
      contentType : "application/json",
      type : "POST",
      data : JSON.stringify(parameter),
      success : (result) => {
        var temp = [];
        console.log(self.state.pickedHero);
        result.played_heroes.forEach((item,i)=>{
          if(self.state.pickedHero.length == 0)
            temp.push(item);
          else{
            for(var i in self.state.pickedHero) {
              if(self.state.pickedHero[i].id == item.id) return;
              if(i == self.state.pickedHero.length-1) temp.push(item);
            }
          }

        })
        self.setState({
          played_heroes : temp,
          key : self.state.key+1,
          currentPlayer : item,
          playerIndex : i
        })
      }
    })
  }

  getWinrate(isAddHistory) {
    var self = this;
    var data = {
      data : [],
      isAddHistory : isAddHistory
    }
    this.state.radiant.forEach((item) => {
      console.log(item);
      var each = {
        player_id : item.player.id,
        hero_id : item.hero.id
      }
      data.data.push(each);
    })
    this.state.dire.forEach((item) => {
      var each = {
        player_id : item.player.id,
        hero_id : item.hero.id
      }
      data.data.push(each);
    })
    var parameter = JSON.stringify(data);
    $.ajax({
      url : Constants.url + "getPlayerHeroWinrate",
      contentType : "application/json",
      type : "POST",
      data : parameter,
      success : (result) => {
        self.setState({
          radiant_winrate : result.radiant,
          dire_winrate : result.dire,
          pickedHero : []
        })
      }
    })
  }

  onSearchChange(e) {
    var value = e.target.value.toLowerCase();
    var filtered = [];
    this.state.backupPlayersData.forEach((item) => {
      if((item.firstname.toLowerCase() + " " + item.lastname.toLowerCase()).indexOf(value) != -1 ||
          item.ingame_name.toLowerCase().indexOf(value) != -1 ||
          item.nation.toLowerCase().indexOf(value) != -1 ||
          item.player_position.toLowerCase().indexOf(value) != -1){
            filtered.push(item);
          }
    })
    this.setState({
      players : filtered,
      key : this.state.key+1
    })
  }

  showModal() {
    var self = this;
    $.get(Constants.url+"getPredictions" , function(result){
      self.setState({
        predictionHistory : result.predictions,
        showModal : true,
        key : self.state.key+1
      })
    })
  }

  closeModal() {
    this.setState({
      showModal : false
    })
  }
  reset() {
    var self = this;
    $.get(Constants.url + "getPlayers" , function(result) {
      self.setState({
        players : result.players,
        backupPlayersData : result.players,
        key : self.state.key+1,
        radiant : [],
        dire : [],
        currentPlayer : {},
        radiant_winrate : 0.00,
        dire_winrate : 0.00,
        pickedHero : []
      })
    });
    this.count = 0;
  }
  historyClicked(item) {
    var self = this;
    var parameter = {id : item.id}
    var radiant = [];
    var dire = [];
    this.count = 100;
    $.ajax({
      url : Constants.url +"getPredictionInfo",
      contentType : "application/json",
      type : "POST",
      data : JSON.stringify(parameter),
      success : function(result) {
        result.data.forEach((item,i)=>{
          var temp = {
            hero : {
              id : item.heroes_id,
              image_url : item.image_url
            },
            player : {
              id : item.player_id,
              ingame_name : item.ingame_name,
              player_position : item.player_position
            }
          }
          if(i<result.data.length/2){
            radiant.push(temp);
          }else{
            dire.push(temp);
          }
        })
        self.setState({
          radiant : radiant,
          dire : dire,
          key : self.state.key+1,
        })
        self.closeModal();
        self.getWinrate(false);
      }
    })
  }

  render() {

    var self = this;
    var playersHeader = ["" , "Ingame name" , "Position" ]
    var heroesHeader = ["" , "Hero name"];
    var pickedHeader = ["" , "Player name" , "Position"];
    var imageStyle = {
      borderRadius : "50%"
    }
    var playerTableDataFunc = (item , i) => {
      return (
        <tr key={i} id="trDataRow" onClick={() => self.playerPicked(item,i)} style={[tableStyle.trDataStyle.base,(i == this.state.playerIndex ? {background : "rgba(244,67,54,0.5)"} : {})]}>
          <td style={tableStyle.tdStyle}><img style={imageStyle} width={40} height={40} src={item.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.ingame_name}</td>
          <td style={tableStyle.tdStyle}>{item.player_position}</td>
        </tr>
      );
    }

    var heroTableDataFunc = (item , i) => {
      return (
        <tr key={i} id="trDataRow" style={tableStyle.trDataStyle.base} onClick={() => self.heroPicked(item)}>
          <td style={tableStyle.tdStyle}><img width={55} height={30} src={item.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.hero_name}</td>
        </tr>
      );
    }

    var pickedTableDataFunc = (item , i) => {
      return (
        <tr key={i} id="trDataRow" style={tableStyle.trDataStyle.base}>
          <td style={tableStyle.tdStyle}><img width={55} height={30} src={item.hero.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.player.ingame_name}</td>
          <td style={tableStyle.tdStyle}>{item.player.player_position}</td>
        </tr>
      );
    }

    var predictionHistoryTableDataFunc = (item , i) => {

      return (
        <tr key={i} id="trDataRow" style={tableStyle.trDataStyle.base} onClick={()=>self.historyClicked(item)}>
          <td style={tableStyle.tdStyle}>{item.winner}</td>
          <td style={tableStyle.tdStyle}>{moment(item.time).format("lll")}</td>
        </tr>
      );
    }

    var listDivStyle = {
      marginTop : 20,
      height : "380px"
    }

    var formControlStyle = {
      backgroundColor : "transparent",
      color : "#FFF"
    }
    var divInfoBlockStyle =  {
      margin : 0,
      display : "inline-block",
      paddingTop : 10,
      width : "48%",
      height : "370px",
      marginLeft : "1%",
      marginRight : "1%",
      background : "rgba(255,255,255,0.15)",
      borderRadius : 8,
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.3)",
      overflowY : "scroll",
      textAlign : "center"
    }

    var divInfoBlockStyleWin =  {
      margin : 0,
      display : "inline-block",
      paddingTop : 10,
      width : "48%",
      height : "370px",
      marginLeft : "1%",
      marginRight : "1%",
      background : "rgba(126,211,33,0.3)",
      borderRadius : 8,
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.3)",
      overflowY : "scroll",
      textAlign : "center"
    }

    var divInfoBlockStyleLose =  {
      margin : 0,
      display : "inline-block",
      paddingTop : 10,
      width : "48%",
      height : "370px",
      marginLeft : "1%",
      marginRight : "1%",
      background : "rgba(244,67,54,0.3)",
      borderRadius : 8,
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.3)",
      overflowY : "scroll",
      textAlign : "center"
    }

    var radiantSpanStyle= {
      color : "rgb(126,211,33)",
      fontWeight : "bold"
    }

    var direSpanStyle= {
      color : "rgb(244,67,54)",
      fontWeight : "bold"
    }

    var modalStyle = {
      position : "absolute",
      padding : "30",
      borderRadius : 8,
      background : "rgba(255,255,255,0.15)",
      color : "#FFF",
      left : "30px",
      right : "30px",
      marginBottom : 30
    }

    var predictionHistoryHeader = ["Winner" , "Time"];
    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
             <div style={contentStyle.divStyle}>
             <Modal show={this.state.showModal} onHide={this.closeModal} style={{background : "#000"}}>
                 <Modal.Body style={modalStyle}>
                   <span>History</span>
                   <div style={contentStyle.dividerStyle}></div>
                   <DataTable key={this.state.key} header={predictionHistoryHeader} data={this.state.predictionHistory} func={predictionHistoryTableDataFunc}/>
                 </Modal.Body>
               </Modal>
               <span>Prediction</span><img width={25} style={{marginLeft : 15}} height={25} src={"./images/recent.svg"} onClick={this.showModal}></img>
               <img width={25} height={25} style={{marginLeft : 15}} src={"./images/refresh.svg"} onClick={this.reset}></img>
               <div style={contentStyle.dividerStyle}></div>
               <div style={listDivStyle}>
                 <Col md={6} id="radiant" style={(this.state.radiant_winrate > this.state.dire_winrate && this.state.radiant_winrate != 0) ? divInfoBlockStyleWin : ((this.state.radiant_winrate == 0) ? divInfoBlockStyle : divInfoBlockStyleLose)}>
                   <span style={radiantSpanStyle}>Radiant</span>
                   <div style={contentStyle.dividerStyle}></div>
                   <span>{"Winrate : " + this.state.radiant_winrate + " %"}</span>
                   <DataTable key={this.state.key} header={pickedHeader} func={pickedTableDataFunc} data={this.state.radiant}/>
                 </Col>
                 <Col md={6} id="dire" style={(this.state.dire_winrate > this.state.radiant_winrate && this.state.radiant_winrate != 0) ? divInfoBlockStyleWin : ((this.state.radiant_winrate == 0) ? divInfoBlockStyle : divInfoBlockStyleLose)}>
                   <span style={direSpanStyle}>Dire</span>
                   <div style={contentStyle.dividerStyle}></div>
                   <span>{"Winrate : " + this.state.dire_winrate + " %"}</span>
                   <DataTable key={this.state.key} header={pickedHeader} func={pickedTableDataFunc} data={this.state.dire}/>
                 </Col>
               </div>
               <div style={{marginTop : 20}}>

                 <FormControl style={formControlStyle} type='text' placeholder="Search player" onChange={this.onSearchChange} />
                 <Col md={6}>
                   <DataTable key={this.state.key} header={playersHeader} func={playerTableDataFunc} data={this.state.players}/>
                 </Col>
                 <Col md={6}>
                   <DataTable key={this.state.key} header={heroesHeader} func={heroTableDataFunc} data={this.state.played_heroes}/>
                 </Col>
               </div>
             </div>
      </ReactCSSTransitionGroup>

    );
  }
}

export default Radium(Prediction);

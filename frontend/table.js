import React from 'react';
import Table from 'react-bootstrap/lib/Table';
import Radium from 'radium';
import tableStyle from './css/tableStyle';

class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.header = this.props.header;
    this.data = this.props.data;
    this.func = this.props.func;
    console.log("2 : " + this.data);
  }

  headerClicked(item) {
    alert(JSON.stringify(item));
  }

  render() {
    var self = this;
    return (
      <div>
      <Table style={tableStyle.tableStyle}>
        <thead style={tableStyle.headerStyle} >
          <tr >
            {
              this.header.map(function(item) {
                return (
                  <th style={tableStyle.thStyle}>{item}</th>
                );
              })
            }
          </tr>
        </thead>
        <tbody>
            {
              this.data.map(this.func)
            }
        </tbody>
      </Table>
      </div>

    );
  }
}

export default Radium(DataTable);

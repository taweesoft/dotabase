import React from 'react';
import ReactDOM from 'react-dom';
import Radium from 'radium';
import Constants from './constants';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import contentStyle from './css/contentStyle';
import DataTable from './table';
import tableStyle from './css/tableStyle';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Player from './player';
import Team from './teamInfo';
class Hero extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data : {},
      whoPlayed : [],
      key :0
    }

    var self = this;
    var loadInfoSuccess = (result) => {
      self.setState({
        data : result.hero_info
      })
    }

    var loadWhoPlayedSuccess = (result) => {
      self.setState({
        whoPlayed : result.players,
        key : 1
      })
    }

    this.loadData = this.loadData.bind(this);
    this.loadData(loadInfoSuccess , "getHeroInfo");
    this.loadData(loadWhoPlayedSuccess , "getWhoPlayedHero");
  }

  loadData(successFunc , route){
    var parameter = {id : this.props.id}
    var self = this;
    $.ajax({
      url : Constants.url + route,
      contentType : "application/json",
      type : "POST",
      data : JSON.stringify(parameter),
      success : successFunc
    });
  }

  player_clicked(item) {
      ReactDOM.render(<Player id={item.player_id}/> , document.getElementById('content'));
  }

  render() {
    var self = this;
    var divBlockWrapperStyle = {
      marginTop : 10,
      alignItems : "stretch"
    }

    var divInfoBlockStyle = {
      margin : 0,
      padding : 10,
      display : "inline-block",
      width: "100%",
      marginTop : "6%",
      marginLeft : "1%",
      marginRight : "1%",
      background : "rgba(255,255,255,0.15)",
      borderRadius : 8,
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.3)"
    }

    var spanNormalStyle = {
      color : "#F95B45",
      fontSize : 35,
      verticalAlign:"middle"
    }

    var imageStyle = {
      borderRadius : "50%"
    }

    var centerRectangleImageStyle = {
      borderRadius : 8,
      marginTop : 20,
      width : "30%" ,
      height : "15%"
    }

    var hero = this.state.data;
    var players = this.state.whoPlayed;
    var tableHeader = ["" , "Name" , "Win / Lose" , "Kill" , "Death" , "Assist" , "GPM" , "XPM"];
    var tableDataFunc = (item,i) => {
      var winLoseStyle = {
        color : item.win_lose == 'lose' ? "#F44336" : "#7ED321"
      }
      return (
        <tr key={i} id="trDataRow" style={tableStyle.trDataStyle.base} onClick={() => self.player_clicked(item)}>
          <td style={tableStyle.tdStyle}><img style={imageStyle} width={40} height={40} src={item.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.ingame_name}</td>
          <td style={[winLoseStyle , tableStyle.tdStyle]}>{item.win_lose}</td>
          <td style={tableStyle.tdStyle}>{item.kills}</td>
          <td style={tableStyle.tdStyle}>{item.death}</td>
          <td style={tableStyle.tdStyle}>{item.assist}</td>
          <td style={tableStyle.tdStyle}>{item.gpm}</td>
          <td style={tableStyle.tdStyle}>{item.xpm}</td>
        </tr>
      );
    }
    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
             <div style={contentStyle.divStyle}>
               <span>Information</span>
               <div style={contentStyle.dividerStyle}></div>
               <div style={{textAlign : "center"}}>
                 <img style={centerRectangleImageStyle} src={hero.image_url}></img>
                 <span style={contentStyle.nameStyle}>{hero.hero_name}</span>
               </div>
               <div style={{marginTop : 20}}>
                 <span>Properties</span>
                 <div style={contentStyle.dividerStyle}></div>
                 <Row style={divBlockWrapperStyle}>
                   <Col id="played" md={3} sm={6} >
                     <div style={divInfoBlockStyle}>
                       <span>Played</span>
                       <div style={contentStyle.dividerStyle}></div>
                       <div style={contentStyle.divInfoSectionStyle}>
                         <span style={contentStyle.spanNormalStyle}>{hero.played}</span>
                       </div>
                     </div>
                   </Col>
                   <Col id="winrate" md={3} sm={6} >
                     <div style={divInfoBlockStyle}>
                       <span>Winrate</span>
                       <div style={contentStyle.dividerStyle}></div>
                       <div style={contentStyle.divInfoSectionStyle}>
                         <span style={contentStyle.spanNormalStyle}>{hero.winrate}</span>
                       </div>

                     </div>
                   </Col>
                   <Col id="winlose" md={3} sm={6}>
                     <div style={divInfoBlockStyle}>
                       <span><span style={{color : "#7ED321"}}>Win</span> | <span style={{color : "#F95B45"}}>Lose</span></span>
                       <div style={contentStyle.dividerStyle}></div>
                       <div style={contentStyle.divInfoSectionStyle}>
                         <span style={contentStyle.spanNormalStyle}><span style={{color : "#7ED321"}}>{hero.win}</span> | <span style={{color : "#F95B45"}}>{hero.lose}</span></span>
                       </div>
                     </div>
                   </Col>
                   <Col id="banned" md={3} sm={6} >
                     <div style={divInfoBlockStyle}>
                       <span style={{color : "#F95B45"}}>Banned</span>
                       <div style={contentStyle.dividerStyle}></div>
                         <div style={contentStyle.divInfoSectionStyle}>
                           <span style={spanNormalStyle}>{hero.banned}</span>
                         </div>
                     </div>
                   </Col>
                 </Row>
               </div>

               <div style={{marginTop : 20}}>
                 <span>{"Who played " + hero.hero_name}</span>
                 <div style={[contentStyle.dividerStyle,{marginBottom : 20}]}></div>
                 <DataTable key={this.state.key} data={this.state.whoPlayed} header={tableHeader} func={tableDataFunc}/>
               </div>
             </div>
      </ReactCSSTransitionGroup>

    );
  }
}

export default Radium(Hero);

import React from 'react';
import ReactDOM from 'react-dom';
import Radium from 'radium';
import Col from 'react-bootstrap/lib/Col';
import Constants from './constants';
import Hero from './hero';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Heroes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data : []
    }
    var self = this;
    $.get(Constants.url + "getHeroes" , function(result){
      self.setState({
        data : result.heroes
      })
    });

  }

  clicked(item) {
    ReactDOM.render(<Hero id={item.id} /> , document.getElementById('content'));
  }

  render() {
    var imgStyle = {
      width : "100%",
      borderRadius : 8,
    }

    var liStyle = {
      display : "inline-block",
      background : "rgba(255,255,255,0.15)",
      padding : 4,
      borderRadius : 8,
      marginTop : "5%",
      color : "#FFF",
      cursor : "pointer",
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.5)"
    }
    var self = this;
    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
             <div>
               {
                 this.state.data.map(function(item , i) {
                   return (
                     <Col md={2} sm={4} xs={6}>
                       <li style={liStyle} onClick={() => self.clicked(item)}>
                       <img style={imgStyle} src={item.image_url}></img>
                       </li>
                     </Col>
                   );
                 })
               }
            </div>
      </ReactCSSTransitionGroup>

    );
  }
}

export default Radium(Heroes);

import React from 'react';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import ReactDOM from 'react-dom';
import Toolbar from './toolbar.js';
import SideBar from './sidebar.js';
import Tournaments from './tournaments';
class App extends React.Component {

  constructor(){
    super();
  }


  render() {
    var containerStyle= {
      marginTop : "50px"
    }

    var sideBarStyle = {
    }

    var contentStyle = {
      position : "fixed",
      padding : 30,
      top : 70,
      right : 0,
      bottom : 0,
      left : "20%",
      overflowY : "scroll"
    }

    var toolBarStyle = {
      position : "fixed",
      top : 0,
      left : 0,
      right : 0,
      width : "100%"
    }
    var divStyle = {
      // background : "rgba(38,50,56,0.8)",
      background : "#F44336",
      width: "20%",
      float: "left",
      position:"absolute",
      top:0,
      bottom :0
    }

    var s = {
      color : "#000"
    }
    return (
      <div>
        <SideBar />
        <Toolbar />
        <div id="content" style={contentStyle}></div>
      </div>
    );
  }
}
// ReactDOM.render(<Toolbar /> , document.getElementById('toolbar'));
ReactDOM.render(<App/> , document.getElementById('container'));
ReactDOM.render(<Tournaments/> , document.getElementById('content'));

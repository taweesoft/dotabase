import React from 'react';
import Radium from 'radium';
import ReactDOM from 'react-dom';
import TeamInfo from './teamInfo';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import contentStyle from './css/contentStyle';
import Col from 'react-bootstrap/lib/Col';
import Constants from './constants';
import moment from 'moment';
import DataTable from './table';
import tableStyle from './css/tableStyle';
class Tournament extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data : {},
      invited_team : [],
      key : 0
    }
    var self = this;
    this.loadData = this.loadData.bind(this);

    var dataSuccessFunc = (result) => {
      self.setState({
        data : result.tournament
      })
    }

    var invitedTeamSuccessFunc = (result) => {
      self.setState({
        invited_team : result.teams,
        key : 1
      })
    }

    this.loadData(dataSuccessFunc, "getTournamentInfo");
    this.loadData(invitedTeamSuccessFunc , "getTeams");
  }

  loadData(successFunc , api){
    var parameter = {id : this.props.id}
    $.ajax({
      url : Constants.url + api,
      contentType : "application/json",
      type : "POST",
      data : JSON.stringify(parameter),
      success : successFunc
    })
  }

  invitedTeamClicked(item) {
    ReactDOM.render(<TeamInfo id={item.id} /> , document.getElementById('content'));
  }

  render() {
    var self = this;
    var tournament = this.state.data;
    var iconStyle = {
      width : "20%",
      height : "20%"
    }
    var headerWrapperStyle = {
      textAlign : "center" ,
      marginTop : 20,
      marginBottom : 20
    }

    var contentWrapperStyle = {
      marginTop : 20,
      marginLeft : 10,
      marginRight : 10
    }
    var mapStyle= {
      width : "100%",
      marginBottom : 10
    }

    var propsBlockStyle = {
      display : "flex",
      flexDirection : "column",
      width : "100%",
      flex : 1,
      verticalAlign: "middle",
      marginBottom : 15
    }

    var spanStyle= {
      fontSize : 20
    }

    var divInfoSectionStyle = { flex : 1 , display : "flex" , alignItems : "center" , justifyContent : "center"}
    var header = ["" , "Team name" , "Ranking" , "Winrate" , "Win" , "Lose"];
    var tableDataFunc = (item , i) => {
      var winStyle = {
        color : "#7ED321"
      }
      var loseStyle = {
        color : "#F44336"
      }
      return (
        <tr key={i} id="trDataRow" style={tableStyle.trDataStyle.base} onClick={() => self.invitedTeamClicked(item)}>
          <td style={tableStyle.tdStyle}><img width={55} height={30} src={item.logo_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.team_name}</td>
          <td style={tableStyle.tdStyle}>{item.position}</td>
          <td style={tableStyle.tdStyle}>{item.winrate}</td>
          <td style={[tableStyle.tdStyle,winStyle]}>{item.win}</td>
          <td style={[tableStyle.tdStyle,loseStyle]}>{item.lose}</td>
        </tr>
      );
    }
    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
      <div style={contentStyle.divStyle}>
        <span>Information</span>
        <div style={contentStyle.dividerStyle}></div>
        <div style={headerWrapperStyle}>
          <img src={tournament.icon_url} style={iconStyle}></img>
          <span style={contentStyle.nameStyle}>{tournament.name}</span>
        </div>
        <div id="detail" style={contentWrapperStyle}>
          <span>Detail</span>
          <div style={contentStyle.dividerStyle}></div>
          <div style={{marginTop : 15 , display : "flex"}}>
            <Col md={5} sm={12} xs={12}>
              <div style={[contentStyle.divInfoBlockStyle , {width : "100%" , height:"100%"}]}>
                <span>Location</span>
                <div style={contentStyle.dividerStyle}></div>
                <div style={headerWrapperStyle}>
                  <img src={tournament.image_url} style={mapStyle}></img>
                  <span>{tournament.location}</span>
                </div>
              </div>
            </Col>
            <Col md={7} sm={12} xs={12} style={{alignItems : "stretch",display : "flex" , flexDirection : "column" }}>
              <div id="prize_pool" style={[contentStyle.divInfoBlockStyle , propsBlockStyle]}>
              <span>Prize pool</span>
              <div style={contentStyle.dividerStyle}></div>
              <div style={divInfoSectionStyle}>
                <span style={spanStyle}>{"$ " + (tournament.prize_pool == undefined ? tournament.prize_pool : tournament.prize_pool.toLocaleString())}</span>
              </div>
              </div>
              <div id="start" style={[contentStyle.divInfoBlockStyle , propsBlockStyle]}>
              <span>Start date</span>
              <div style={contentStyle.dividerStyle}></div>
              <div style={divInfoSectionStyle}>
                <span style={spanStyle}>{moment(tournament.start_date).format("ll")}</span>
              </div>
              </div>
              <div id="end" style={[contentStyle.divInfoBlockStyle , propsBlockStyle , {marginBottom : 0}]}>
              <span>End date</span>
              <div style={contentStyle.dividerStyle}></div>
              <div style={divInfoSectionStyle}>
                <span style={spanStyle}>{moment(tournament.end_date).format("ll")}</span>
              </div>
              </div>
            </Col>
          </div>
        </div>
        <div style={contentWrapperStyle}>
          <span>Invited teams</span>
          <div style={contentStyle.dividerStyle}></div>
          <DataTable key={this.state.key} header={header}  data={this.state.invited_team} func={tableDataFunc}/>
        </div>
      </div>
      </ReactCSSTransitionGroup>
    );
  }
}

export default Radium(Tournament);

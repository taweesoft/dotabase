module.exports = Object.freeze({
   headerStyle : {
    background : "rgba(255,255,255,0.15)",
    borderTop : "0px solid #FFF",
    borderBottom: "0px solid #fff"
  },

   tableStyle : {
    marginTop : 15,
    borderCollapse: "separate",
    borderTop : "0px solid #FFF",
    borderBottom: "0px solid #fff"
  },

   thStyle : {
    borderTop : "0px solid #FFF",
    borderBottom: "0px solid #fff"
  },

   tdStyle : {
    borderTop : "0px solid rgba(255,255,255,0.2)",
    borderBottom: "1px solid rgba(255,255,255,0.25)",
    paddingTop : "10px",
    paddingBottom : "10px",
    verticalAlign : "middle"
  },

   trDataStyle : {
    base : {
      ':hover' : {
        background : "rgba(244,67,54,0.5)"
      },
      transition : "0.1s",
      cursor : "pointer"
    }
  }
})

module.exports = Object.freeze({
  divStyle : {
    position : "absolute",
    padding : "30",
    borderRadius : 8,
    background : "rgba(255,255,255,0.15)",
    color : "#FFF",
    left : "30px",
    right : "30px",
    marginBottom : 30
  },
  dividerStyle : {
    marginTop : 4,
    height : 1,
    width : "100%",
    background : "rgba(255,255,255,0.5)"
  },
  divBlockWrapperStyle : {
    marginTop : 30,
    display : "flex",
    alignItems : "stretch"
  },
  centerRectangleImageStyle : {
    borderRadius : 8,
    marginTop : 20,
    width : "50%" ,
    height : "20%"
  },
  nameStyle : {
    display : "block",
    marginTop : 20,
    fontSize : 17
  },
  divInfoBlockStyle : {
    margin : 0,
    padding : 10,
    display : "inline-block",
    width : "31%",
    marginLeft : "1%",
    marginRight : "1%",
    background : "rgba(255,255,255,0.15)",
    borderRadius : 8,
    "box-shadow": "0px 0px 15px rgba(0,0,0,0.3)"
  },
  spanNormalStyle : {
    fontSize : 35,
    verticalAlign:"middle"
  },
  divInfoSectionStyle : {
    textAlign : "center",
    marginTop : 20,
    marginBottom : 20,
    verticalAlign : "middle",
    height : "auto"
  },
  formControlStyle : {
    marginBottom : 20,
    backgroundColor : "transparent",
    color : "#FFF"
  }
})

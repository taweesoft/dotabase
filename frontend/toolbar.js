import React from 'react';
import Radium from 'radium';
import Col from 'react-bootstrap/lib/Col';

class Toolbar extends React.Component{
  render() {
    var colStyle = {
      margin : 0,
      padding : "0 0 0 20px",
      height : 70,
      background : "rgba(38,50,56,1)",
      display : "flex",
      alignItems : "center",
      "box-shadow": "0px 1px 5px #000"
    }

    var textStyle = {
      fontSize : "20px"
    }
    return(
      <Col sm={12} md={12} xs={12} lg={12} style={colStyle}>
        <span style={textStyle}><span style={{color : "#F44336"}}>DOTA</span><span style={{color : "#FFF"}}>BASE</span></span>
      </Col>
    );
  }
}

export default Radium(Toolbar);

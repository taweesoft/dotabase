import React from 'react';
import ReactDOM from 'react-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Constants from './constants';
import Radium from 'radium';
import Tournament from './tournament';
class Tournaments extends React.Component {
  constructor(props) {
    super(props);
    console.log("tournaments constructor");
    this.state = {
      data : []
    }
    var self = this;
    $.get(Constants.url+"getTournaments" , function(result) {
      self.setState({
        data : result.tournaments
      });
    });
  }

  clicked(item) {
    ReactDOM.render(<Tournament id={item.id} />  , document.getElementById('content'));
  }

  render() {
    var self = this;
    var ulStyle = {
      margin : 0,
      padding : 0
    }

    var liStyle = {
      position : "relative",
      cursor : "pointer",
      display : "flex",
      background : "rgba(255,255,255,0.15)",
      borderRadius : 8,
      marginTop : 50,
      marginRight : 100,
      marginBottom : 50,
      marginLeft : 100,
      padding : 20,
      color : "#FFF",
      alignItems : "center",
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.5)"
    }

    var pHeaderStyle = {
      fontSize : "2.5em"
    }

    var pStyle = {
      fontSize : 17
    }

    var imgStyle = {
      width : "20%"
    }

    var divPWrapperStyle = {
      display : "inline-block"
    }

    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
            <div>
             <ul style={ulStyle} >
               {
                 this.state.data.map(function(item , i){
                   return (
                     <li style={liStyle} onClick={()=>self.clicked(item)}>
                       <img style={imgStyle} src={item.image_url}></img>
                       <img responsive style={{marginLeft : 25 , marginRight : 25 , width:"10%"}} src={item.icon_url}></img>
                       <div style={divPWrapperStyle}>
                         <p style={pHeaderStyle} >{item.name}</p>
                         <p style={pStyle} >{item.location}</p>
                       </div>
                     </li>
                   );
                 })
             }
             </ul>
            </div>
      </ReactCSSTransitionGroup>
    );
  }
}

export default Radium(Tournaments);

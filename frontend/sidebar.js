import React from 'react';
import ReactDOM from 'react-dom';
import Radium from 'radium';
import Tournaments from './tournaments.js';
import Players from './players.js';
import Teams from './teams.js';
import Heroes from './heroes.js';
import Constants from './constants';
import Prediction from './prediction';

class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.items = [
      {
        title : "Tournaments",
        icon : "./images/trophy.svg"
      },
      {
        title : "Teams",
        icon : "./images/team.svg"
      },
      {
        title : "Heroes" ,
        icon : "./images/star.svg"
      },
      {
        title : "Players",
        icon : "./images/joystick.svg"
      },
      {
        title : "Prediction",
        icon : './images/prediction.svg'
      }
    ];
    this.state = {
      active : 0
    }
  }

  clicked(i) {
    this.setState({ active : i});
    switch(i) {
      case 0 :
        console.log('rendering tournaments');
        ReactDOM.render(<Tournaments/> , document.getElementById('content'));
        break;
      case 1 :
        ReactDOM.render(<Teams/> , document.getElementById('content'));
        break;
      case 2 :
        ReactDOM.render(<Heroes /> , document.getElementById('content'));
        break;
      case 3 :
        console.log("rendering players");
        ReactDOM.render(<Players/> , document.getElementById('content')) ;
        break;
      case 4 :
        ReactDOM.render(<Prediction/> , document.getElementById('content')) ;
        break;
    }
  }


  render() {
    var divStyle = {
      "box-shadow": "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
    }

    var ulStyle = {
      background : "rgba(38,50,56,0.8)",
      listStyle : "none",
      margin : 0,
      padding: 0,
      width: "20%",
      position:"fixed",
      top:70,
      bottom :0
    }

    var iconStyle = {
      marginLeft : 15,
      marginRight : 15
    }
    var self = this;
    return (
      <div style={divStyle}>
          <ul style={ulStyle}>
            {
              this.items.map(function(item,i){
                var style = {
                  ':hover' : {
                    background : "rgba(244,67,54,0.5)"
                  },
                  transition : "0.1s",
                  "background" : (self.state.active == i) ? "#F44336" : 0,
                  "padding" : "10px 0 10px 0",
                  paddingLeft : 15,
                  cursor : "pointer"
                }
                return (
                  <li key={i} style={style} onClick={() => self.clicked(i)}><img style={iconStyle} width={15} height={15} src={item.icon}></img><span style={{color : "#FFF"}}>{item.title}</span></li>
                );
              })
            }
          </ul>
      </div>
    );
  }
}

export default Radium(SideBar);

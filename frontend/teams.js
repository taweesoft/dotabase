import React from 'react';
import ReactDOM from 'react-dom';
import Radium from 'radium';
import Row from 'react-bootstrap/lib/Row';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import TeamInfo from './teamInfo.js';
import Constants from './constants';

class Teams extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data :[]
    }
    this.loadData = this.loadData.bind(this);
    this.sortByAscending = this.sortByAscending.bind(this);
    this.sortByDecending = this.sortByDecending.bind(this);
    this.loadData(1);
  }

  loadData(option) {
    var self = this;
    var parameter = {option : option};
    $.ajax({
      data : JSON.stringify(parameter),
      url : Constants.url + "getTeams",
      contentType : "application/json",
      type : "POST",
      success : (result)=>{
        self.setState({
          data : result.teams
        })
      }
    });
  }

  sortByAscending(props) {
    this.loadData(1);
  }

  sortByDecending(props) {
    this.loadData(2);
  }

  clicked(item) {
    ReactDOM.render(<TeamInfo id={item.id} /> , document.getElementById('content'));
  }

  render() {
    var self = this;
    var divStyle = {
    }

    var ulStyle = {
      margin : 0,
      padding : 0,
      listStyle : "none"
    }
    var liStyle = {
      display : "inline-block",
      background : "rgba(255,255,255,0.15)",
      padding : 10,
      width : "40%",
      marginLeft : "5%",
      marginRight : "5%",
      borderRadius : 8,
      marginTop : "5%",
      color : "#FFF",
      cursor : "pointer",
      "box-shadow": "0px 0px 15px rgba(0,0,0,0.5)"
    }

    var spanStyle = {
      display : "inline-block",
      marginTop : 10,
      marginBottom : 10,
      fontSize : 17
    }

    var imgStyle = {
      width : "100%",
      borderRadius : 8
    }

    var iconStyle = {
      width : 30,
      height : 30,
      marginLeft : 10,
      marginRight : 10,
      display : "inline-block"
    }

    var rowStyle = {
      float : "right" ,
      textAlign : "right"
    }

    var dropdownButtonStyle = {
      marginRight : 60,
      color : "#FFF",
      background : "Transparent"
    }
    console.log(this.state.data);
    return (
      <ReactCSSTransitionGroup transitionName = "example"
             transitionAppear = {true} transitionAppearTimeout = {500}
             transitionEnter = {true} transitionLeave = {true}>
            <div>
             <Row>
               <ButtonToolbar style={rowStyle}>
                 <DropdownButton bsSize="small" style={dropdownButtonStyle} title="Sort by" id="dropdown-size-small">
                   <MenuItem onClick={this.sortByAscending}>Winner to Loser</MenuItem>
                   <MenuItem onClick={this.sortByDecending}>Loser to Winner</MenuItem>
                 </DropdownButton>
               </ButtonToolbar>
             </Row>
             <ul style={ulStyle}>
               {
                 this.state.data.map((item , i) => {
                   return (
                     <li style={liStyle} onClick={() => self.clicked(item)}>
                       <div>
                         <img style={iconStyle} src={item.logo_url}></img>
                         <span style={spanStyle}>{item.team_name}</span>
                       </div>
                       <img style={imgStyle} src={item.image_url}></img>
                     </li>
                   );
                 })
               }
             </ul>
            </div>
      </ReactCSSTransitionGroup>

    );
  }
}

export default Radium(Teams);

import React from 'react';
import ReactDOM from 'react-dom';
import Table from 'react-bootstrap/lib/Table';
import DataTable from './table';
import Radium from 'radium';
import Player from './player.js';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Constants from './constants';
import contentStyle from './css/contentStyle';
import tableStyle from './css/tableStyle';
import FormControl from 'react-bootstrap/lib/FormControl';
class Players extends React.Component {
  constructor(props) {
    super(props);
    this.header = ["","Name" , "Ingame name" , "Nationality" , "Position"];
    this.state = {
      data : [],
      backupData : [],
      key : 0
    }
    this.clicked = this.clicked.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);

    var self = this;
    $.get(Constants.url+"getPlayers" , function(result) {
      self.setState({
        data : result.players,
        backupData : result.players,
        key : 1
      });
    });
  }

  clicked(item) {
    ReactDOM.render(<Player id={item.id}/> , document.getElementById('content'));
  }

  onSearchChange(e) {
    var value = e.target.value.toLowerCase();
    var filtered = [];
    this.state.backupData.forEach((item) => {
      if((item.firstname.toLowerCase() + " " + item.lastname.toLowerCase()).indexOf(value) != -1 ||
          item.ingame_name.toLowerCase().indexOf(value) != -1 ||
          item.nation.toLowerCase().indexOf(value) != -1 ||
          item.player_position.toLowerCase().indexOf(value) != -1){
            filtered.push(item);
          }
    })
    this.setState({
      data : filtered,
      key : this.state.key+1
    })
  }

  render() {
    console.log("rendering");
    console.log(this.state.data);
    var self = this;
    var click = (item)=>{
      this.clicked(item);
    }

    var imageStyle = {
      borderRadius : "50%"
    }
    var tableDataFunc = (item , i) => {
      return (
        <tr key={i} id="trDataRow" onClick={() => self.clicked(item)} style={tableStyle.trDataStyle.base}>
          <td style={tableStyle.tdStyle}><img style={imageStyle} width={40} height={40} src={item.image_url}></img></td>
          <td style={tableStyle.tdStyle}>{item.firstname + " " + item.lastname}</td>
          <td style={tableStyle.tdStyle}>{item.ingame_name}</td>
          <td style={tableStyle.tdStyle}>{item.nation}</td>
          <td style={tableStyle.tdStyle}>{item.player_position}</td>
        </tr>
      );
    }
    return (
        <ReactCSSTransitionGroup transitionName = "example"
               transitionAppear = {true} transitionAppearTimeout = {500}
               transitionEnter = {true} transitionLeave = {true}>
              <div style={contentStyle.divStyle}>
              <FormControl style={contentStyle.formControlStyle} type='text' placeholder="Search" onChange={this.onSearchChange} />
              <DataTable key={this.state.key} header={this.header} data={this.state.data} func={tableDataFunc} />
              </div>
        </ReactCSSTransitionGroup>
    );
  }
}
export default Radium(Players);

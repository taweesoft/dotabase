var fs = require('fs');
var host = "taweesoft.xyz";
var port = 8080;
var express = require('express');

var app = express();
var cors = require('cors')
var bodyParser = require('body-parser')
app.use(cors());
app.use(bodyParser.json());
var query = require('./query.js');

app.get("/dotabase/getTournaments",function(request , response){
  var callback = function(data){
    console.log("----- Get tournaments -----");
    response.status(200).send(data);
  };
  query.getTournaments(callback);
});

app.post('/dotabase/getTeams' , function(request , response) {
  var info = request.body;
  console.log("option : " + info.option);
  var callback = function(data) {
    console.log('---- Get teams ----');
    response.status(200).send(data);
  }
  query.getTeams(callback,info);
});

app.get('/dotabase/getPlayers', function(request, response) {
  var callback = function(data) {
    console.log('---- Get all players ----');
    response.status(200).send(data);
  }
  query.getPlayers(callback);
});

app.post('/dotabase/getPlayerInfo', function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get player info id : ' + info.id + ' ----');
    response.status(200).send(data);
  }
  query.getPlayerInfo(callback,info);
});

app.post('/dotabase/getPlayedHero' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get played hero of player id : ' + info.id + ' ----');
    response.status(200).send(data);
  }
  query.getPlayedHero(callback , info);
});

app.post('/dotabase/getTeamInfo' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get team infomation of id : ' + info.id + " ----");
    response.status(200).send(data);
  }
  query.getTeamInfo(callback ,info);
});

app.post('/dotabase/getTeamMembers' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log("---- Get team member of taem id : " + info.id + " -----");
    response.status(200).send(data);
  }
  query.getTeamMembers(callback , info);
});

app.get('/dotabase/getHeroes' , function(request , response) {
  var callback = function(data) {
    console.log('---- Get all heroes ----');
    response.status(200).send(data);
  }
  query.getHeroes(callback);
});

app.post('/dotabase/getWhoPlayedHero' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get who played hero of id : ' + info.id + " ----" );
    response.status(200).send(data);
  }
  query.getWhoPlayedHero(callback , info);
});

app.post('/dotabase/getHeroInfo' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get hero info of id : ' + info.id + ' ----');
    response.status(200).send(data);
  }
  query.getHeroInfo(callback , info);
});

app.post('/dotabase/getTournamentInfo' , function(request,  response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get tounament info of id : ' + info.id + ' ----');
    response.status(200).send(data);
  }
  query.getTournamentInfo(callback , info);
});

app.post('/dotabase/getPlayerHeroWinrate' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get player hero winrate ----');
    response.status(200).send(data);
  }
  query.getPlayerHeroWinrate(callback , info);
})

app.post('/dotabase/getPlayedHeroDistinct' , function(request , response) {
  var info = request.body;
  var callback = function(data) {
    console.log('---- Get played heroes distinct ----');
    response.status(200).send(data);
  }
  query.getPlayedHeroDistinct(callback , info);
})

app.get('/dotabase/getPredictions' , function(request , response) {
  var callback = function(data) {
    console.log('---- Get predictions ----');
    response.status(200).send(data);
  }
  query.getPredictions(callback);
})

app.post('/dotabase/getPredictionInfo' , function(request,  response) {
  var info = request.body;
  var callback = function(data) {
    console.log("---- Get prediction info ----");
    response.status(200).send(data);
  }
  query.getPredictionInfo(callback , info);
})

app.listen(port,host);
console.log("Server running on port " + port);

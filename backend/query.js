var mysql = require('mysql');

var connection;
function handleDisconnect() {
  connection  = mysql.createConnection({
    host : "localhost",
    user : "root",
    password : "superR@@T",
    database : "dotabase"
  });                                              // the old one cannot be reused.

  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

exports.getTournaments = function(callback){
  connection.query("SELECT * FROM tournaments",function(err, rows,fields){
    if(err) throw err;
    var data  = {
      tournaments : rows
    }
    callback(data);
  });
}

exports.getTeams = function(callback,info) {
  var option = info.option;
  var query = "SELECT * FROM team ORDER BY position" + (option == 2 ? " DESC" : "");
  connection.query(query , function(err , rows , fields) {
    if(err) throw err;
    var data = {
      teams : rows
    }
    callback(data);
  })
}

exports.getPlayers = function(callback) {
  connection.query("SELECT * from players" , function(err ,rows ,fields) {
    if(err) throw err;
    var data = {
      players : rows
    }
    callback(data);
  })
}

exports.getPlayerInfo = function(callback , info) {
  var id = info.id;
  connection.query("SELECT DISTINCT players.* ,team.team_name ,team.winrate ,team.logo_url , (SELECT count(*) FROM players_with_hero WHERE player_id="+id+" AND win_lose='lose') as lose , (SELECT count(*) FROM players_with_hero WHERE player_id="+id+" AND win_lose='win') as win FROM players, players_in_team , team , players_with_hero WHERE players.id=players_in_team.player_id AND players_in_team.team_id=team.id AND players_with_hero.player_id = players.id AND players.id="+id , function(err ,rows , fields) {
    if(err) throw err;
    var data = {
      player : rows[0]
    }
    callback(data);
  })
}

exports.getPlayedHero = function(callback , info) {
  var id = info.id;
  connection.query("SELECT players_with_hero.* , heroes.hero_name , heroes.image_url from players_with_hero,heroes WHERE heroes.id = players_with_hero.hero_id AND player_id="+id , function(err , rows,fields) {
    if(err) throw err;
    var data = {
      played_hero : rows
    }
    callback(data);
  })
}

exports.getTeamInfo = function(callback , info) {
  var id = info.id;
  connection.query("SELECT * FROM team WHERE id="+id , function(err , rows,fields) {
    if(err) throw err;
    var data = {
      team : rows[0]
    }
    callback(data);
  })
}

exports.getTeamMembers = function(callback , info) {
  var id = info.id; //team id
  connection.query("SELECT players.* FROM players , players_in_team WHERE players.id = players_in_team.player_id AND players_in_team.team_id=" + id , function(err, rows,fields) {
    if(err) throw err;
    var data = {
      members : rows
    }
    callback(data);
  })
}

exports.getHeroes = function(callback) {
  connection.query("SELECT * FROM heroes", function(err , rows , fields) {
    if(err) throw err;
    var data = {
      heroes : rows
    }
    callback(data);
  })
}

exports.getWhoPlayedHero = function(callback , info) {
  var id = info.id;
  connection.query("SELECT players.ingame_name , players.image_url , players_with_hero.* FROM players , players_with_hero WHERE players.id = players_with_hero.player_id AND players_with_hero.hero_id="+id , function(err , rows,fields) {
    if(err) throw err;
    var data = {
      players : rows
    }
    callback(data);
  })
}

exports.getHeroInfo = function(callback , info) {
  var id = info.id;
  connection.query("SELECT * FROM heroes WHERE id=" + id , function(err , rows , fields ) {
    if(err) throw err;
    var data = {
      hero_info : rows[0]
    }
    callback(data);
  })
}

exports.getTournamentInfo = function(callback , info) {
  var id = info.id;
  connection.query("SELECT * FROM tournaments WHERE id=" + id , function(err ,rows , fields) {
    if(err) throw err;
    var data = {
      tournament : rows[0]
    }
    callback(data);
  })
}

exports.getPlayerHeroWinrate = function(callback , info) {
  var radiant = 0;
  var dire = 0;
  var halfLength = info.data.length/2;
  var isAddHistory = info.isAddHistory;
  info.data.forEach((item,i) => {
    var player_id = item.player_id;
    var hero_id = item.hero_id;
    connection.query("SELECT (SELECT count(*) from players_with_hero WHERE player_id="+player_id+" AND hero_id="+hero_id+" group by win_lose having win_lose='lose') as lose , (SELECT count(*) from players_with_hero WHERE player_id="+player_id+" AND hero_id="+hero_id+" group by win_lose having win_lose='win') as win   FROM players_with_hero group by lose,win",function(err,rows,fields) {
      if(err) throw err;
      var win = rows[0].win;
      var lose = rows[0].lose;
      connection.query("SELECT winrate FROM heroes WHERE id="+hero_id , function(err , rows, fields) {
        if(err) throw err;
        var heroWinrate = rows[0].winrate;
        var averageWinrate = heroWinrate * (win*1.0)/(win+lose);
        if(i<halfLength) {
          radiant += averageWinrate;
        }else{
          dire += averageWinrate;
        }
        if(i==info.data.length-1){
          var radiantWinrate = (radiant/halfLength).toFixed(2);
          var direWinrate = (dire/halfLength).toFixed(2);
          var data = {
            radiant : radiantWinrate,
            dire : direWinrate
          }
          if(isAddHistory){
            var winner = (radiantWinrate >= direWinrate) ? "Radiant" : "Dire";
            connection.query("INSERT INTO prediction (winner) VALUES (\'"+winner+"\')" , function(err ,rows ,fields) {
              if(err) throw err;
              var id = rows.insertId;
              info.data.forEach((item,i) => {
                var team = (i<halfLength) ? "Radiant" : "Dire";
                connection.query("INSERT INTO prediction_history (prediction_id,player_id,hero_id,team) VALUES (\'"+id+"\',\'"+item.player_id+"\',\'"+item.hero_id+"\',\'"+team+"\')" , function(err,rows,fields){
                  if(err) throw err;
                })
              })
            })
          }
          callback(data);
        }
      })
    })

  })
}

exports.getPlayedHeroDistinct = function(callback , info) {
  var id = info.id;
  connection.query("SELECT heroes.id , heroes.hero_name , heroes.image_url from players_with_hero,heroes WHERE heroes.id = players_with_hero.hero_id AND player_id="+id+" group by heroes.id" , function(err , rows , fields) {
    if(err) throw err;
    var data = {
      played_heroes : rows
    }
    callback(data);
  })
}

exports.getPredictions = function(callback) {
  connection.query("SELECT * FROM prediction ORDER BY id DESC" , function(err,rows,fields) {
    if(err) throw err;
    var data = {
      predictions : rows
    }
    callback(data);
  })
}

exports.getPredictionInfo = function(callback,info) {
  var data = {
    data : []
  }
  var id = info.id;
  connection.query("SELECT * FROM prediction_history WHERE prediction_id="+id , function(err,rows,fields) {
    if(err) throw err;
    var length = rows.length;
    rows.forEach((item,i) => {
      connection.query("SELECT players.id as player_id , players.ingame_name ,players.player_position , heroes.image_url , heroes.id as heroes_id , heroes.hero_name FROM players,prediction_history , heroes WHERE players.id=prediction_history.player_id AND heroes.id=prediction_history.hero_id AND prediction_history.player_id="+item.player_id+" AND prediction_history.hero_id="+item.hero_id, function(err2 ,rows2,fields2) {
        if(err2) throw err2;
        data.data.push(rows2[0]);
        if(i==length-1){
          callback(data);
        }
      })
    })
  })
}
